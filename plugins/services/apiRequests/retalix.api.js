// import axios from "axios";
// const trainUrl = "trains/";
// export const RetalixApiService = {
//   async getSearch(from_city, to_city, checkout_time, adult_count, children_count, tour_operator = null) {
//     return (
//       await axios.get(
//         `${trainUrl}search?from_city=${from_city.toString()}&to_city=${to_city.toString()}&checkout_time=${checkout_time}&adult_count=${adult_count}&children_count=${children_count}&tour_operator=${tour_operator}`,
//       )
//     ).data;
//   },
//   async book(orders, id) {
//     return (
//       await axios.post(`${trainUrl}booking/`, {
//         operator_id: id,
//         orders: orders,
//       })
//     ).data;
//   },
//   async getWagonDetailToday(id) {
//     return (await axios.get(`${trainUrl}today-condition/wagon/${id}/`)).data;
//   },
//   async createWagon(wagon) {
//     return (await axios.post(`${trainUrl}wagons/`, wagon)).data;
//   },
//   async updateWagon(id, wagon) {
//     return (await axios.put(`${trainUrl}wagons/${id}/`, wagon)).data;
//   },
//   async deleteWagon(id) {
//     return (await axios.delete(`${trainUrl}wagons/${id}/`)).data;
//   },
// };