export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'supportFront',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: "stylesheet",
        href:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
      },
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"
      }
    ],

    script: [
      {
        src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
        type: "text/javascript"
      },
      {
        src:
          "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js",
        type: "text/javascript"
      },
      {
        src:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js",
        type: "text/javascript"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // '@/assets/style.css'
  ],

  router: {
    middleware: ['clearValidationErrors']
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    './plugins/mixins/user.js',
    './plugins/axios.js',
    './plugins/mixins/validation.js',
    './plugins/eventBus.js',
    // './plugins/globals',
    // './plugins/services/api.service',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/dotenv'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/vuetify',
    'nuxtjs-mdi-font'
  ],

  // Axios module config
  axios: {
    baseURL: process.env.API_URL,
    // need to change to this -> process.env.URL_API, and in .env file, need to write
    // smth like URL_API=http://127.0.0.1:8000/api 
  },

  // Auth module config
  // auth: {
  //   strategies: {
  //     local: {
  //       endpoints: {
          // login: {
          //   url: 'login',
          //   method: 'post',
          //   propertyName: 'meta.token',
          // },
          // user: {
          //   url: 'user',
          //   method: 'get',
          //   // propertyName: 'data',
          // },
          // logout: {
          //   url: 'logout',
          //   method: 'post',
          // }
  //       },
  //     }
  //   },
  // },

  auth: {
    strategies: {
      'laravelJWT': {
        provider: 'laravel/jwt',
        url: '/',
        endpoints: {
          login: {
            url: 'auth/login',
            method: 'post',
            propertyName: false,
          },
          user: {
            url: 'auth/user',
            method: 'get',
            propertyName: false,
          },
          logout: {
            url: 'auth/logout',
            method: 'post',
            propertyName: false,
          }
        },
        token: {
          property: 'access_token',
          maxAge: 60 * 60
        },
        refreshToken: {
          maxAge: 20160 * 60
        },
      },
    }
  },




//   auth: {
//     strategies:    {
//         local: {
//             endpoints:    {
//               login: {
//                 url: 'login',
//                 method: 'post',
//               },
//               user: {
//                 url: 'user',
//                 method: 'get',
//               },
//               logout: {
//                 url: 'logout',
//                 method: 'post',
//               }
//             },
//             autoLogout:   false,
//             user:         {
//                 property:  false,
//                 autoFetch: true,
//             },
//         },
//     },
//     watchLoggedIn: true,
// },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
