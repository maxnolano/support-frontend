export default {
    agents(state) {
      return state.agents;
    },
    headers_cancelled(state) {
      return state.headers_cancelled;
    },
    cities(state) {
      return state.cities;
    },
    clients(state) {
      return state.clients;
    },
    stores(state) {
      return state.stores;
    }
  };
  