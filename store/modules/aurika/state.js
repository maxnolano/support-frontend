export default () => ({
    headers: [
      { text: 'id', value: 'id' },
      { text: 'store_id', value: 'store_id' },
      { text: 'st_title', value: 'st_title' },
      { text: 'pump_number', value: 'pump_number' },
      { text: 'product_price', value: 'product_price' },
      { text: 'payment_type', value: 'payment_type' },
      { text: 'status', value: 'status' },
      { text: 'create_date', value: 'create_date' },
      { text: 'modify_date', value: 'modify_date' },
      { text: "", value: "controls", sortable: false },
  ],
  headers_cancelled: [
    { text: 'c_id', value: 'c_id' },
    { text: 'st_title', value: 'st_title' },
    { text: 'c_order_id', value: 'c_order_id' },
    { text: 'c_litre', value: 'c_litre' },
    { text: 'c_store_id', value: 'c_store_id' },
    { text: 'c_cancel_reason', value: 'c_cancel_reason' },
    { text: 'c_ext_order_id', value: 'c_ext_order_id' },
    { text: 'c_ext_date', value: 'c_ext_date' },
    { text: 'c_create_date', value: 'c_create_date' },
    { text: 'c_status', value: 'c_status' }
  ],
  cities: [
    {label: 'Астана', id: 1},
    {label: 'Атбасар', id: 2},
    {label: 'Балкашино', id: 3},
    {label: 'Экибастуз', id: 4},
    {label: 'Есиль', id: 5},
    {label: 'Сандыктау', id: 6},
    {label: 'Степногорск', id: 7}
  ],
  cities: [
    {username: 'Stepa.us', id: -1}
  ],
  agents: [
    {label: 'Kaspi', id: 9},
    {label: 'Halyk', id: 8},
    {label: 'Смартзаправка', id: 1},
    {label: 'Alacard', id: 10}
  ],
  stores: [],
  });
  