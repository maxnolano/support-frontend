export default {
    setHeaders(state, val) {
      state.headers = val;
    },
    setCities(state, val) {
      state.cities = val;
    },
    setClients(state, val) {
      state.clients = val;
    },
    setStores(state, val) {
      state.stores = val;
    },
  };
  