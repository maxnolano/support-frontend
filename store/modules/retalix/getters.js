export default {
    headers(state) {
      return state.headers;
    },
    headers_cancelled(state) {
      return state.headers_cancelled;
    },
    statuses(state) {
      return state.statuses;
    },
    headers_kst(state) {
      return state.headers_kst;
    },
    headers_cancelled_kst(state) {
      return state.headers_cancelled_kst;
    },
    statuses_kst(state) {
      return state.statuses_kst;
    }
  };
  