export default {
    setHeadersAction({ commit }, val) {
      commit("setHeaders", val);
    },
    setCitiesAction({ commit }, val) {
      commit("setCities", val);
    },
    setStoresAction({ commit }, val) {
      commit("setStores", val);
    },
  };
  