export default () => ({
  cities: [
    {label: 'Алматы', id: 3},
    {label: 'Астана', id: 5},
    {label: 'Актобе', id: 2},
    {label: 'Кызылорда', id: 12},
    {label: 'Все города', id: -1}
  ],
  agents: [
    {label: 'Kaspi', id: 9},
    {label: 'Halyk', id: 8},
    {label: 'Смартзаправка', id: 1},
    {label: 'Alacard', id: 10}
  ],
  stores: [],
  statuses: [],
  });
  