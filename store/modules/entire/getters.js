export default {
    agents(state) {
      return state.agents;
    },
    headers_cancelled(state) {
      return state.headers_cancelled;
    },
    cities(state) {
      return state.cities;
    },
    stores(state) {
      return state.stores;
    },
    statuses(state) {
      return state.statuses;
    },
  };
  